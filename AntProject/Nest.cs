﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntProject
{
    /// <summary>
    /// An ant nest that the user can create additional instances of anywhere in the world;
    /// Ant objects will search for nest instances, finding their location when they enter the instance's radius;
    /// If the ant objects are carrying food, they will move towards the the nest instance and when they occupy
    /// the same tile, they will deposit the food they are carrying into the instance;
    /// </summary>
    class Nest
    {

        private Point nestLocation;//location of the nest instance


        private int radius = 16;//detection radius ants can see nest instances from


        private int depositedFood = 0;//amount of food deposited to nest -this currently is just used for debugging and has no effects

        //nest instances are constructed at the coordinates the user clicked at on the worldpanel
        public Nest (int xPos, int yPos)
        {
            nestLocation.X = xPos;
            nestLocation.Y = yPos;
        }

        //accessor method for ant objects to compare the distance between themselves and a nest instance, and the detection radius of the nest instance
        public int GetRadius()
        {
            return radius;
        }

        //accessor method for the location of a nest instance
        public Point GetLocation()
        {
            return nestLocation;
        }

        /// <summary>
        /// Checks whether an ant can deposit food to the nest, and if it can, make it do so
        /// </summary>
        /// <param name="ant">the ant trying to deposit food to the nest</param>
        public void DepositFood(Ant ant)
        {
            bool antCarryingFood = ant.GetCarryingFood();//check whether the ant is carrying food

            //if the ant has food to deposit
            if (antCarryingFood == true)
            {
                //ant deposits food to nest
                ant.DropFood();
                depositedFood++;
            }
        }
    }
}
