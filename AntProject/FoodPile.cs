﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntProject
{
    /// <summary>
    /// A pile of food that the user can create additional instances of anywhere in the world;
    /// Ant objects will search for foodpile instances, finding their location when they enter the instance's radius;
    /// If the ant objects are not carrying any food, they will move towards the food pile instance and when they occupy the 
    /// same tile, they will take one piece of food from the instance;
    /// Once the instance has no more pieces of food, it is destroyed.
    /// </summary>
    class FoodPile
    {

        private Point foodLocation;//location of the food pile


        private int radius = 10;//detection radius ants can see a food pile from

        private int remainingFood = 100;//the finite amount of food a new food pile instance is initialised with

        //food pile instances are constructed with the coordinates a user clicked at on the world panel
        public FoodPile (int xPos, int yPos)
        {
            foodLocation.X = xPos;
            foodLocation.Y = yPos;
        }

        /*accessor method for an ant object to compare the distance between
         *itself and a foodpile instance, with the foodpile's detection radius
         */
        public int GetRadius()
        {
            return radius;
        }

        //accessor method for the location of a foodpile instance
        public Point GetLocation()
        {
            return foodLocation;
        }

        /// <summary>
        /// Checks if there is any remaining food in the food pile,
        /// If there is, checks that the ant object that is sharing a tile with the pile pick up any food,
        /// If it can, the ant object picks up food and the food in the pile decrements;
        /// Called by AntForm when an ant object is sharing a tile with a foodpile instance;
        /// </summary>
        /// <param name="ant">ant object sparing a tile with an instance of foodpile</param>
        public void TakeFood(Ant ant)
        {
            //if there is food remaining
            if (remainingFood > 0)
            {
                bool antCarryingFood = ant.GetCarryingFood();//find out if the ant is carrying food

                //if the ant isn't already carrying food
                if (antCarryingFood == false)
                {

                    ant.PickedUpFood();//ant picks up food

                    remainingFood--;//food taken from pile
                }

            }
        }

        /// <summary>
        /// Checks remainingFood and returns true if there is no food left;
        /// Returns false is there is food left;
        /// </summary>
        /// <returns></returns>
        public bool isEmpty()
        {
            //if no food is remaining
            if (remainingFood <= 0)
            {
                //pile is empty
                return true;
            }
            //if there is some food remaining
            else
            {
                //pile is not empty
                return false;
            }
        }

        
    }
}
