﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AntProject
{
    /// <summary>
    /// The Form class the creates and initialises all objects of the other classes,
    /// handles input, draws the visual representation of the simulation and calls the 
    /// simulation methods for collision and random movement in ant, nest and food objects
    /// </summary>
    public partial class AntForm : Form
    {

        private Random randomObject; //random object to pass to ant, nest and food objects when determining their initial coordinates

        private bool isRunning = true;
        Timer loopTimer;
        
        private static int loopsPerSecond = 20;
        private static int loopDelay = 1000 / loopsPerSecond;

        private Ant[] antArray;         //array to store all Ant objects
        private BanditAnt[] banditAntArray;         //array to store all BanditAnt objects
        private List<FoodPile> foodList = new List<FoodPile>();        //list to store all FoodPile objects
        private List<Nest> nestList = new List<Nest>();//list to store all Nest objects
        private List<BanditNest> banditNestList = new List<BanditNest>();//list to store all BanditNest objects

        private Bitmap simImage;        //bitmap to draw the simulation visuals onto

        private bool displayGrid = false;        //boolean for whether or not to display gridlines on background of panel

        //the width and height of the world to simulate on the panel
        private int worldWidth;
        private int worldHeight;

        /// <summary>
        /// The width and height of a tile, which world height and width are divided by
        /// The size of one ant. Used to measure distance and size relative to that
        /// </summary>
        private int tileSize;

        private string clickMode = "food"; //what object will be placed when the user clicks on the panel. Default to placing a FoodPile object

        /// <summary>
        /// Extend Control.CreateParams class to add WS_EX_COMPOSITED style to window;
        /// WS_EX_COMPOSITED will double buffer the AntForm window
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                //create object of CreateParams class
                CreateParams cp = base.CreateParams;

                //Turn on WS_EX_COMPOSITED to double buffer painting
                cp.ExStyle |= 0x02000000;

                return cp;
            }
        }

        /// <summary>
        /// Constructor for ant form to simulate world in;
        /// initialises the size of the world, the size of tiles and the image to display on the panel;
        /// calls the SpawnAnts() method to create the ants;
        /// initialises paint and click event handlers to panel;
        /// initialises background worker for handling the simulation loop.
        /// </summary>
        public AntForm()
        {
            InitializeComponent();


            randomObject = new Random();//create random object

            //set world size to size of the form panel
            worldWidth = worldPanel.Width;
            worldHeight = worldPanel.Height;


            tileSize = 5;//set Tile size

            simImage = new Bitmap(worldWidth, worldHeight);//create bitmap the size of the world to create

            SpawnAnts();//initialise ants

            SpawnBanditAnts();//initialise bandit ants


            worldPanel.Paint += new PaintEventHandler(worldPanel_Paint);//assign Paint event handler to world panel

            worldPanel.Click += new System.EventHandler(worldPanel_Click);//add new click event handler to world panel

            //create new timer that we will use to call main simulation loop
            loopTimer = new Timer();
            loopTimer.Interval = loopDelay;
            loopTimer.Tick += new EventHandler(loopTimer_Tick);

        }

        /// <summary>
        /// This method initialises all the ant objects and gives them a random position on the map
        /// </summary>
        private void SpawnAnts()
        {
            int numberofAnts = 300;//set the number of ants to create

            antArray = new Ant[numberofAnts];//create array to hold ants

            int numberOfTilesX = (worldWidth / tileSize), numberOfTilesY = (worldHeight / tileSize); //split the world panel up into a number of small tiles

            /*
             * for each empty index in the array, set a random x and y tile coordinate
             * and then initialise the ant object at that tile
             */
            for (int i = 0; i < antArray.Length; i++)
            {
                int xPos = randomObject.Next(0, numberOfTilesX) * tileSize;
                int yPos = randomObject.Next(0, numberOfTilesY) * tileSize;

                /* Initialise the Ant object:
                 * Pass tileSize and world size to the ant as well as its random coordinates;
                 * The tileSize is passed so that the ant knows how many pixels to move
                 * each time step in order to move one tile, without having to access AntForm's tileSize property;
                 * worldWidth and worldHeight are passed so that the ant knows when it has reached the end of the world
                 * and needs to wrap around to the other side without having to access AntForm's world size properties.
                 * Accessing AntForm's properties is avoided because there is a large amount of Ant objects and it is CPU intensive to 
                 * have every single one create its own AntForm object just to access these properties.
                 */
                antArray[i] = new Ant(tileSize, worldWidth, worldHeight, xPos, yPos);
            }
        }

        private void SpawnBanditAnts()
        {
            int numberofBanditAnts = 25;//set the number of bandit ants to create

            banditAntArray = new BanditAnt[numberofBanditAnts];//create array to hold bandit ants

            int numberOfTilesX = (worldWidth / tileSize), numberOfTilesY = (worldHeight / tileSize); //split the world panel up into a number of small tiles

            /*
             * for each empty index in the array, set a random x and y tile coordinate
             * and then initialise the ant object at that tile
             */
            for (int i = 0; i < banditAntArray.Length; i++)
            {
                int xPos = randomObject.Next(0, numberOfTilesX) * tileSize;
                int yPos = randomObject.Next(0, numberOfTilesY) * tileSize;

                /* Initialise the Ant object:
                 * Pass tileSize and world size to the ant as well as its random coordinates;
                 * The tileSize is passed so that the ant knows how many pixels to move
                 * each time step in order to move one tile, without having to access AntForm's tileSize property;
                 * worldWidth and worldHeight are passed so that the ant knows when it has reached the end of the world
                 * and needs to wrap around to the other side without having to access AntForm's world size properties.
                 * Accessing AntForm's properties is avoided because there is a large amount of Ant objects and it is CPU intensive to 
                 * have every single one create its own AntForm object just to access these properties.
                 */
                banditAntArray[i] = new BanditAnt(tileSize, worldWidth, worldHeight, xPos, yPos);
            }
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            //start running the program
            isRunning = true;
            loopTimer.Enabled = true;

            stopButton.Enabled = true;
            startButton.Enabled = false;//stop user from pressing button again
        }

        private void stopButton_Click(object sender, EventArgs e)
        {

            isRunning = false;

            startButton.Enabled = true;//re-enable start button
            stopButton.Enabled = false;
        }

        void loopTimer_Tick(object sender, EventArgs e)
        {
            //simulate one time step on tick if required
            if (isRunning)
            {
                SimLoop();
            }
        }

        /// <summary>
        /// This method simulates and resolves all arguments for every ant, nest and foodpile object for one time step
        /// </summary>
        private void SimLoop()
        {
            /*forgetful ants
            occasionally an ant will forget where food or nest is*/
            int randomAnt = randomObject.Next(0, antArray.Length);//pick a random ant
            int antMemory = antArray[randomAnt].GetMemory();//get the "memory" value for that one ant
            
            //based on the memory value of that ant, determine what the forgetful ant will forget
            if (antMemory == 1)
            {
                antArray[randomAnt].ForgetFood();//make the ant forget where the food pile is
            }
            else if (antMemory == 2)
            {
                antArray[randomAnt].ForgetNest();//make this one ant forget its nest
            }
            else if (antMemory == 3)
            {
                //make this one ant forget everything
                antArray[randomAnt].ForgetFood();
                antArray[randomAnt].ForgetNest();
            }

            /*forgetful bandit ants
             * do the same for bandit ants
             */
            int randomBanditAnt = randomObject.Next(0, banditAntArray.Length);//pick a random bandit ant
            int banditAntMemory = banditAntArray[randomBanditAnt].GetMemory();//get the "memory" value for that one bandit ant

            //based on the memory value of that bandit ant, determine what the forgetful bandit ant will forget
            if (banditAntMemory == 1)
            {
                banditAntArray[randomBanditAnt].ForgetRegularAntCarryingFood();//make the bandit ant forget where the regular ant carrying food is
            }
            else if (banditAntMemory == 2)
            {
                banditAntArray[randomBanditAnt].ForgetBanditNest();//make this one bandit ant forget its nest
            }
            else if (banditAntMemory == 3)
            {
                //make this one bandit ant forget everything
                banditAntArray[randomBanditAnt].ForgetRegularAntCarryingFood();
                banditAntArray[randomBanditAnt].ForgetBanditNest();
            }

            //for every ant in the ant array
            for (int i = 0; i < antArray.Length; i++)
            {
                /*
                 * Simulate the ant's movement:
                 * movement is slightly randomised using random value passed to ant;
                 * The random object is passed to the ant instead of defined by it because otherwise all the ants would
                 * base their random movement on the same random seed
                 */
                antArray[i].Simulate(randomObject.Next(-1,2));

                
                /* 
                 * Compare ant[i] to every other ant in the array with a greater index value
                 * This is to make it so that any two ants do not make the same comparison twice,
                 * once between ant[i] and ant[j] and then again between ant[j] and ant[i];
                 */
                for (int j = i + 1; j < antArray.Length; j++)
                {
                    //if ant[i] is in radius of ant[j]
                    if (antArray[i].isInRadius(antArray[j]) == true)
                    {
                        antArray[i].Communicate(antArray[j]);//make ant[i] communicate with ant[j]
                    }
                }

                //compare ant[i] to every FoodPile object
                for (int f = 0; f < foodList.Count; f++)
                {
                    //if ant is in radius of food pile
                    if (antArray[i].isInRadius(foodList[f]) == true)
                    {
                        antArray[i].LearnFood(foodList[f].GetLocation());//ant learns food pile location
                    }

                    //if the ant is within a tile of the pile
                    if (antArray[i].isTouching(foodList[f]) == true)
                    {

                        foodList[f].TakeFood(antArray[i]);//check if the ant can take food from the pile, and if so make it do so

                        //if there is now no food remaining in the pile
                        if (foodList[f].isEmpty())
                        {
                            foodList.RemoveAt(f);//delete the food pile
                        }
                    }
                }

                //compare ant[i] to every nest object
                for (int n = 0; n < nestList.Count; n++)
                {
                    //if ant is in radius of the nest
                    if (antArray[i].isInRadius(nestList[n]) == true)
                    {
                        antArray[i].LearnNest(nestList[n].GetLocation());//ant learns nest location
                    }

                    //if the ant is within a tile of the nest
                    if (antArray[i].isTouching(nestList[n]) == true)
                    {
                        nestList[n].DepositFood(antArray[i]); //check if the ant can deposit food the nest, and if so do so
                    }
                }

            }

            //for every bandit ant in the array
            for (int i = 0; i < banditAntArray.Length; i++)
            {
                /*
                 * Simulate the bandit ant's movement:
                 * movement is slightly randomised using random value passed to bandit ant;
                 * The random object is passed to the bandit ant instead of defined by it because otherwise all the bandit ants would
                 * base their random movement on the same random seed
                 */
                banditAntArray[i].Simulate(randomObject.Next(-1, 2));


                /* 
                 * Compare banditAnt[i] to every other bandit ant in the array with a greater index value
                 * This is to make it so that any two bandit ants do not make the same comparison twice,
                 * once between banditAnt[i] and banditAnt[j] and then again between banditAnt[j] and banditAnt[i];
                 */
                for (int j = i + 1; j < banditAntArray.Length; j++)
                {
                    //if banditAnt[i] is in radius of banditAnt[j]
                    if (banditAntArray[i].isInRadius(banditAntArray[j]) == true)
                    {
                        banditAntArray[i].Communicate(banditAntArray[j]);//make banditAnt[i] communicate with banditAnt[j]
                    }
                }

                //compare banditAnt[i] to every regular ant object
                for (int r = 0; r < antArray.Length; r++)
                {
                    //if bandit ant is in radius of regular ant
                    if (banditAntArray[i].isInRadius(antArray[r]) == true)
                    {
                        banditAntArray[i].CheckRegularAntHasFood(antArray[r]);//bandit ant checks whether regular ant is carrying food and learns location if so
                    }

                    //if the bandit ant is within a tile of the regular ant, the regular ant is carrying food and the bandit ant isn't
                    if (banditAntArray[i].isTouching(antArray[r]) == true)
                    {
                        if (antArray[r].GetCarryingFood() == true)
                        {
                            if (banditAntArray[i].GetCarryingFood() == false)
                            {
                                banditAntArray[i].StealFood(antArray[r]);//the bandit ant steals food from the regular ant
                            }
                        }
                    }
                }

                //compare banditAnt[i] to every nest object
                for (int n = 0; n < banditNestList.Count; n++)
                {
                    //if bandit ant is in radius of the nest
                    if (banditAntArray[i].isInRadius(banditNestList[n]) == true)
                    {
                        banditAntArray[i].LearnBanditNest(banditNestList[n].GetLocation());//bandit ant learns bandit nest location
                    }

                    //if the bandit ant is within a tile of the bandit nest
                    if (banditAntArray[i].isTouching(banditNestList[n]) == true)
                    {
                        banditNestList[n].DepositFood(banditAntArray[i]); //check if the bandit ant can deposit food to the bandit nest, and if so do so
                    }
                }
            }

            worldPanel.Invalidate(); //call the worldPanel_Paint event handler so that the new state of each object is shown visually
        }

        /// <summary>
        /// Called on application start and every time step to update graphics on worldPanel
        /// </summary>
        private void worldPanel_Paint(object sender, PaintEventArgs e)
        {
            Draw(); //draw the new graphics onto the worldPanel
        }

        private void worldPanel_Click(object sender, EventArgs e)
        {
            Point clickPoint = worldPanel.PointToClient(Cursor.Position);//get position of mouse cursor when the panel is clicked

            //get x and y coordinates of mouse click and round them to the nearest tile coordinate
            int pointX = clickPoint.X - (clickPoint.X % tileSize);
            int pointY = clickPoint.Y - (clickPoint.Y % tileSize);

            //determine what action to complete at the cursor position based on current clickMode
            if (clickMode == "food")
            {
                //create new foodpile at cursor position
                foodList.Add(new FoodPile(pointX, pointY));
            }
            else if (clickMode == "nest")
            {
                //create new nest at cursor position
                nestList.Add(new Nest(pointX, pointY));
            }
            else if (clickMode == "banditNest")
            {
                //create new bandit nest at cursor position
                banditNestList.Add(new BanditNest(pointX, pointY));
            }
        }

        /// <summary>
        /// Wipes worldPanel clean and then re-draws background elements and every ant, nest and food pile object at it's position
        /// </summary>
        private void Draw()
        {

            int antSize = 1 * tileSize;//size to draw each ant
            
            int foodPileSize = 4 * tileSize;//size to draw each food pile
            
            int nestSize = 6 * tileSize;//size to draw each nest

            
            //initialise all the below graphics objects and then dispose of them once the statement is complete
            using (Graphics simImageGraphics = Graphics.FromImage(simImage))//using the simulation bitmap as a canvas to draw onto
            using (Brush antBrush = new SolidBrush(Color.Black))//using a black brush to draw all the ant objects
            using (Brush banditAntBrush = new SolidBrush(Color.Red))//using a red brush to draw all the bandit ant objects
            using (Brush foodBrush = new SolidBrush(Color.Green))//using a brown brush to draw all the food pile objects
            using (Brush nestBrush = new SolidBrush(Color.DarkGray))//using a dark gray brush to draw all the nest objects
            using (Brush banditNestBrush = new SolidBrush(Color.Maroon))//using a maroon brush to draw all the bandit nest objects
            using (Pen gridPen = new Pen(Color.LightGray))//using a light gray pen to draw gridlines onto the background image
            {

                simImageGraphics.Clear(Color.Cornsilk);//wipe bitmap clean and paint new beige background

                //if the user has toggled gridlines on, draw gridlines
                if (displayGrid)
                {
                    //draw a vertical grid line all the way along the width of the world, with a gap the size of a tile between each
                    for (int i = 1; i < (worldWidth / tileSize); i++)
                    {
                        simImageGraphics.DrawLine(gridPen, tileSize * i, 0, tileSize * i, worldHeight);//draw the line onto the bitmap
                    }
                    //do the same horizontally
                    for (int i = 1; i < (worldHeight / tileSize); i++)
                    {
                        simImageGraphics.DrawLine(gridPen, 0, tileSize * i, worldWidth, tileSize * i);//draw the line onto the bitmap
                    }
                }

                //draw every nest object that currently exists
                foreach (Nest nest in nestList)
                {
                    Point location = nest.GetLocation();//get the nest's location

                    //paint a filled circle centered on the nest's location on bitmap
                    simImageGraphics.FillEllipse(nestBrush, location.X - (nestSize / 2), location.Y - (nestSize / 2), nestSize, nestSize);
                }

                //draw every bandit nest object that currently exists
                foreach (BanditNest banditNest in banditNestList)
                {
                    Point location = banditNest.GetLocation();//get the bandit nest's location

                    //paint a filled circle centered on the bandit nest's location on bitmap
                    simImageGraphics.FillEllipse(banditNestBrush, location.X - (nestSize / 2), location.Y - (nestSize / 2), nestSize, nestSize);
                }

                //draw every food pile object that currently exists
                foreach (FoodPile pile in foodList)
                {
                    Point location = pile.GetLocation();//get it's location

                    //Paint a filled circle centered on it's location on bitmap
                    simImageGraphics.FillEllipse(foodBrush, location.X - (foodPileSize / 2), location.Y - (foodPileSize / 2), foodPileSize, foodPileSize);
                }
                
                //paint all the ant objects that exist
                foreach (Ant ant in antArray)
                {
                    Point location = ant.GetLocation();//get the ant's location

                    //paint the ant onto bitmap at those coordinates
                    simImageGraphics.FillEllipse(antBrush, location.X, location.Y, antSize, antSize);
                }

                //paint all the bandit ant objects that exist
                foreach (BanditAnt banditAnt in banditAntArray)
                {
                    Point location = banditAnt.GetLocation();//get the bandit ant's location

                    //paint the bandit ant onto bitmap at those coordinates
                    simImageGraphics.FillEllipse(banditAntBrush, location.X, location.Y, antSize, antSize);
                }

                //now that the bitmap is painted, trace the bitmap onto the panel
                using (Graphics panelGraphics = worldPanel.CreateGraphics())
                {
                    panelGraphics.DrawImage(simImage, 0, 0, worldWidth, worldHeight);
                }

            }

        }

        private void foodButton_Click(object sender, EventArgs e)
        {
            //set click mode to food
            clickMode = "food";

            //disable food button, renable others
            foodButton.Enabled = false;
            nestButton.Enabled = true;
            banditNestButton.Enabled = true;
        }

        private void nestButton_Click(object sender, EventArgs e)
        {
            //set click mode to nest
            clickMode = "nest";

            //disable nest button, enable others
            nestButton.Enabled = false;
            foodButton.Enabled = true;
            banditNestButton.Enabled = true;
        }

        private void banditNestButton_Click(object sender, EventArgs e)
        {
            //set click mode to bandit nest
            clickMode = "banditNest";

            //disable banditNestButton, enable others
            banditNestButton.Enabled = false;
            foodButton.Enabled = true;
            nestButton.Enabled = true;
        }

        private void helpButton_Click(object sender, EventArgs e)
        {
            //display dialog box with instructions
            MessageBox.Show("Press Start to start the simulation and Stop to pause it. Click on the simulation to place either a pile of food or a nest, based on what button is clicked above. Black ants will search for food together look for a nest to carry it back to. Red bandit ants will search for black ants carrying food and steal it from them to take it back to their own nests.", "Instructions");
        }

        private void toggleGrid_Click(object sender, EventArgs e)
        {
            displayGrid ^= true;//toggle whether to display gridlines or not
        }

        

    }
}
