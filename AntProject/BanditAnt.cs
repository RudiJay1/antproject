﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntProject
{   
    /// <summary>
    /// A bandit ant that the AntForm class creates a set amount of instances of on application start;
    /// Each bandit ant instance will move in one of 8 surrounding directions every time step;
    /// The bandit ant instance starts out with no knowledge of the location of any regular ant objects carrying food or bandit nest objects and isn't carrying any food;
    /// The bandit ant instance moves around the world randomly until it comes into detection range of a regular ant object carrying food or bandit nest object, 
    /// in which case it will remember the location;
    /// When an bandit ant instance is in range of another bandit ant instance, the two instances will share whatever knowledge they have of existing
    /// regular ant objects carrying food or bandit nest object locations;
    /// When a bandit ant instance isn't carrying food, it will search for a regular ant object carrying food, or return to one it knows the location of to steal food;
    /// When a bandit ant instance is carrying food, it will search for a bandit nest object, or return to one it knows the location of to drop off the food;
    /// </summary>
    class BanditAnt
    {
        private Point location; //the x and y position of the bandit ant instance

        //tile size and world size passed from AntForm
        private int tile;
        private int widthBoundary;
        private int heightBoundary;

        //booleans for determining bandit ant instance's current goal (find regular ant object carrying food or find bandit nest)
        private bool isCarryingFood = false;
        private bool remembersRegularAntCarryingFood = false;
        private bool remembersBanditNest = false;

        //instance's last known coordinates of regular ant objects carrying food and bandit nest objects
        private Point regularAntCarryingFoodLocation;
        private Point banditNestLocation;


        private int currentDirection;//which of the 8 directions a bandit ant instance is currently moving in

        //the eight directions mapped to numeric values so that they can be incremented and decremented in
        private int north = 1, northeast = 2, east = 3, southeast = 4, south = 5, southwest = 6, west = 7, northwest = 8;


        private int antMemory = 3;//a counter that determines what a randomly picked bandit ant instance will forget based on its value


        private int radius = 5;//the maximum number of tiles two bandit ant instances can have between them and still communicate what they know

       /* Construct the BanditAnt object:
        * A bandit ant instance is passed the current tileSize and world size of the world it was initialised in
        * as well as randomised x and y coordinates.
        */
        public BanditAnt(int tileSize, int worldWidth, int worldHeight, int xPos, int yPos)
        {
            tile = tileSize;
            widthBoundary = worldWidth;
            heightBoundary = worldHeight;

            location.X = xPos;
            location.Y = yPos;
        }

        /// <summary>
        /// The method called by AntForm to simulate a bandit ant instance's behaviour in one time step;
        /// Cycle's bandit ant instance's memory counter to determine what it forgets if it is randomly picked by AntForm to forget something;
        /// Tells a bandit ant instance that if it is at the known location of a regular ant object carrying food and hasn't picked anything up, the regular ant is no longer there;
        /// Determines whether a bandit ant instance should face towards a known regular ant object carrying food, face towards a known bandit nest or search for one of those by facing in random directions,
        /// and determines what direction a bandit ant instance should face towards in order to do so;
        /// Moves the bandit ant in the direction it is facing;
        /// </summary>
        /// <param name="randomVal">
        /// Random value passed from AntForm rather that created in BanditAnt class so that every bandit ant instance has a unique random seed
        /// </param>
        public void Simulate(int randomVal)
        {

            antMemory--;//bandit ant memory decreases

            
            if (antMemory == 0)//if bandit ant memory reaches zero cycle it back around
            {
                antMemory = 3;
            }

            /*if a bandit ant instance is within a tile of the regular ant object carrying food's location they remember, and they haven't picked up food
            then assume there is no food or the ant is gone left, forget the location and move on*/
            if (location.X <= regularAntCarryingFoodLocation.X + tile && location.X > regularAntCarryingFoodLocation.X - tile)
            {
                if (location.Y < regularAntCarryingFoodLocation.Y + tile && location.Y > regularAntCarryingFoodLocation.Y - tile)
                {
                    if (isCarryingFood == false)
                    {
                        remembersRegularAntCarryingFood = false;
                    }
                }
            }

            //if a bandit ant instance is not carrying food but remembers where a regular ant object carrying food is, move towards the ant carrying food
            if (isCarryingFood == false && remembersRegularAntCarryingFood == true)
            {
                MoveTowards(regularAntCarryingFoodLocation.X, regularAntCarryingFoodLocation.Y, randomVal);//pass the location of the regular ant carrying food and the randomVal object on
            }
            //if a bandit ant instance is carrying food and remembers where a bandit nest object is, move towards the bandit nest
            else if (isCarryingFood == true && remembersBanditNest == true)
            {
                MoveTowards(banditNestLocation.X, banditNestLocation.Y, randomVal);//pass the location of the target and randomVal object on
            }
            //if a bandit ant instance doesn't know where to go next, just move in a random direction until it finds something
            else
            {
                RandomMovement(randomVal);
            }

        }

        /// <summary>
        /// Changes a bandit ant instance's current direction so that they are roughly facing their target;
        /// </summary>
        /// <param name="xPos">x coordinate of the target</param>
        /// <param name="yPos">y coordinate of the target</param>
        /// <param name="randomVal">Random modifier to cycle the direction up or down one, or stay the same</param>
        public void MoveTowards(int xPos, int yPos, int randomVal)
        {
            
            /*
             a bandit ant instance's currentDirection is set to one of three directions:
             * exact direction towards target (randomVal == 0)
             * direction towards target but slightly off to the left (randomVal == -1)
             * direction towards target but slightly off to the right (randomVal == 1)
            */
            //if target is southeast
            if (location.X < xPos && location.Y < yPos)
            {
                currentDirection = southeast + randomVal;
            }
            //if target is south 
            else if (location.X == xPos && location.Y < yPos)
            {
                currentDirection = south + randomVal;
            }
            //if target is southwest
            else if (location.X > xPos && location.Y < yPos)
            {
                currentDirection = southwest + randomVal;
            }
            //if target is west
            else if (location.X > xPos && location.Y == yPos)
            {
                currentDirection = west + randomVal;
            }
            //if target is northwest
            else if (location.X > xPos && location.Y > yPos)
            {
                currentDirection = northwest + randomVal;
            }
            //if target is north
            else if (location.X == xPos && location.Y > yPos)
            {
                currentDirection = north + randomVal;
            }
            //if target is northeast
            else if (location.X < xPos && location.Y > yPos)
            {
                currentDirection = northeast + randomVal;
            }
            //if target is east
            else if (location.X < xPos && location.Y == yPos)
            {
                currentDirection = east + randomVal;
            }

            //complete circle of directions
            if (currentDirection < north)
            {
                currentDirection = northwest;
            }
            else if (currentDirection > northwest)
            {
                currentDirection = north;
            }


            Move();//move in direction decided
            
        }

        /// <summary>
        /// Increments or decrements a bandit ant instance's current direction, or keeps it the same to emulate more natural random movement
        /// rather than just having the bandit ant instance move randomly in any of eight directions regardless of where it was last going
        /// </summary>
        /// <param name="randomVal">random modifier determine whether to increment or decrement current direction, or stay the same</param>
        public void RandomMovement(int randomVal)
        {
            //add 1, 0 or -1 to currentDirection to adjust it slightly left, stay on course or adjust slightly right
            currentDirection += randomVal;

            //complete circle of eight directions
            if (currentDirection < north)
            {
                currentDirection = northwest;
            }
            if (currentDirection > northwest)
            {
                currentDirection = north;
            }


            Move();//move in direction decided
        }

        /// <summary>
        /// Move a bandit ant instance forward in whatever of the eight possible directions it is currently facing;
        /// If a bandit ant instance moves off the edge of the screen, wrap it around to the other side.
        /// </summary>
        public void Move()
        {
            //change a bandit ant instance's position coordinates based on the current direction it is facing in
            switch (currentDirection)
            {
                //move north
                case 1:
                    location.Y -= tile;
                    break;
                //move northwest
                case 2:
                    location.Y -= tile;
                    location.X += tile;
                    break;
                //move west
                case 3:
                    location.X += tile;
                    break;
                //move southwest
                case 4:
                    location.X += tile;
                    location.Y += tile;
                    break;
                //move south
                case 5:
                    location.Y += tile;
                    break;
                //move southeast
                case 6:
                    location.Y += tile;
                    location.X -= tile;
                    break;
                //move east
                case 7:
                    location.X -= tile;
                    break;
                //move northeast
                case 8:
                    location.X -= tile;
                    location.Y -= tile;
                    break;
            }

            //if a bandit ant instance moves off the edge of the world, wrap it around to the other side
            if (location.X < 0)
            {
                location.X = widthBoundary - tile;
            }
            if (location.X == widthBoundary)
            {
                location.X = 0;
            }
            if (location.Y < 0)
            {
                location.Y = heightBoundary - tile;
            }
            if (location.Y == heightBoundary)
            {
                location.Y = 0;
            }
        }

        /// <summary>
        /// Compares the distance between two bandit ant instances and returns if they are within radius of each other
        /// </summary>
        /// <param name="otherBanditAnt">The other bandit ant instance</param>
        public bool isInRadius(BanditAnt otherBanditAnt)
        {
            //the distance between this bandit ant instance and the other
            Point otherAntLocation = otherBanditAnt.GetLocation();
            int distX = (location.X - otherAntLocation.X) / tile;
            int distY = (location.Y - otherAntLocation.Y) / tile;

            //if the distance between bandit ant instances is less than the radius
            if (distX <= radius && distX > -radius && distY <= radius && distY > -radius)
            {

                return true;//the bandit ant instances are in radius of each other
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Compares the distance between a bandit ant instance and a regular ant object and determines if the bandit ant is in the regular ant's radius
        /// </summary>
        public bool isInRadius(Ant regularAnt)
        {
            
            Point regularAntLocation = regularAnt.GetLocation();//the food pile object's coordinates

            //the distance between this ant instance and the pile
            int distX = (location.X - regularAntLocation.X) / tile;
            int distY = (location.Y - regularAntLocation.Y) / tile;

            //if the distance between ant and pile is less than the pile object's radius
            if (distX <= regularAnt.GetDetectRadius() && distX > -regularAnt.GetDetectRadius() && distY <= regularAnt.GetDetectRadius() && distY > -regularAnt.GetDetectRadius())
            {
                return true;//the bandit ant is in the regular ant's radius
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Compares the distance between a bandit ant instance and a regular ant object and determines
        /// whether they are within one tile of each other
        /// </summary>
        public bool isTouching(Ant regularAnt)
        {

            Point regularAntLocation = regularAnt.GetLocation();//the regular ant object's coordinates

            //the distance between a bandit ant instance and a regular ant object
            int distX = (location.X - regularAntLocation.X) / tile;
            int distY = (location.Y - regularAntLocation.Y) / tile;

            //if the distance between bandit ant and regular ant is less than one tile
            if (distX <= tile && distX > -tile && distY <= tile && distY > -tile)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Compares the distance between a bandit ant instance and a bandit nest object and determines if the bandit ant is in the bandit nest's radius
        /// </summary>
        public bool isInRadius(BanditNest banditNest)
        {
            //determine the distance between a bandit ant instance and the bandit nest
            Point newBanditNestLocation = banditNest.GetLocation();
            int distX = (location.X - banditNestLocation.X) / tile;
            int distY = (location.Y - banditNestLocation.Y) / tile;

            //if the distance between the bandit ant and the bandit nest is less than the radius
            if (distX <= banditNest.GetRadius() && distX > -banditNest.GetRadius() && distY <= banditNest.GetRadius() && distY > -banditNest.GetRadius())
            {
                return true;//the bandit ant is in the bandit nest's radius
            }
            else
            {
                return false;
            }
            
        }


        /// <summary>
        /// Compares the distance between a bandit ant instance and a bandit nest object and determines
        /// whether they are within one tile of each other
        /// </summary>
        public bool isTouching(BanditNest banditNest)
        {
            //the distance between a bandit ant and the bandit nest
            Point banditNestLocation = banditNest.GetLocation();
            int distX = (location.X - banditNestLocation.X) / tile;
            int distY = (location.Y - banditNestLocation.Y) / tile;

            //if the distance between a bandit ant and the bandit nest is less than a tile
            if (distX <= tile && distX > -tile && distY <= tile && distY > -tile)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// One bandit ant instance tells another the location of a regular ant object carrying food and/or bandit nest object if one knows the location of one
        /// and the other doesn't.
        /// </summary>
        /// <param name="otherBanditAnt">Another bandit ant instance</param>
        public void Communicate(BanditAnt otherBanditAnt)
        {
            /* if this bandit ant instance knows where a regular ant object carrying food is
             * and the other bandit ant instance doesn't
             */
            if (remembersRegularAntCarryingFood == true)
            {
                if (otherBanditAnt.GetRegularAntCarryingFoodKnowledge() == false)
                {
                    /*
                     * If the regular ant object carrying food location this bandit ant instance knows isn't the last location the other bandit ant instance remembered, teach it;
                     * (Stops the other bandit ant instance from following this one to regular ant object carrying food that it just found out no longer exists there).
                     */
                    if (otherBanditAnt.GetFood() != regularAntCarryingFoodLocation)
                    {
                        otherBanditAnt.LearnRegularAntCarryingFood(regularAntCarryingFoodLocation);//access setter method for regular ant carrying object coordinates in the other bandit ant instance
                    }
                }
            }
            //if this bandit ant instance doesn't know where a regular ant carrying food is but the other bandit ant instance does
            else
            {
                if (otherBanditAnt.GetRegularAntCarryingFoodKnowledge() == true)
                {
                    /*
                     * If the regular ant carrying food location the other bandit ant instance knows isn't the last location this bandit ant instance remembered, learn from it;
                     * (Stops this bandit ant instance from following the other one to a regular ant carrying food that it just found out no longer exists there).
                     */
                    if (otherBanditAnt.GetFood() != regularAntCarryingFoodLocation)
                    {

                        remembersBanditNest = true;
                        
                        regularAntCarryingFoodLocation = otherBanditAnt.GetFood(); //access getter method for regular ant carrying food object coordinates from other bandit ant instance
                    }
                }
            }

            //if this bandit ant instance knows the location of a bandit nest object and the other bandit ant instance doesn't, teach it
            if (remembersBanditNest == true)
            {
                if (otherBanditAnt.GetBanditNestKnowledge() == false)
                {
                    otherBanditAnt.LearnBanditNest(banditNestLocation);//access setter method for bandit nest object coordinates in the other bandit ant instance
                }
            }
            //if this bandit ant doesn't know where the bandit nest is but the other bandit ant instance does, learn from it
            else
            {
                if (otherBanditAnt.GetBanditNestKnowledge() == true)
                {
                    remembersBanditNest = true;

                    banditNestLocation = otherBanditAnt.GetBanditNest();//access getter method for bandit nest object coordinates in other bandit ant instance
                }
            }
        }

        /// <summary>
        /// When a bandit ant is in the same tile as a regular ant carrying food, bandit ant instance steals the food from the regular ant
        /// </summary>
        /// <param name="regularAnt"></param>
        public void StealFood(Ant regularAnt)
        {
            isCarryingFood = true;
            regularAnt.DropFood(); //regular ant "drops" the food it was carrying
        }

        /// <summary>
        /// Setter method for a bandit ant instance losing food
        /// </summary>
        public void DropFood()
        {
            isCarryingFood = false;
        }

        /// <summary>
        /// Setter method for a bandit ant checking whether a regular ant is carrying food, and if it is learning it's location
        /// </summary>
        /// <param name="regularAnt">A regular ant object a bandit ant instance is in radius of</param>
        public void CheckRegularAntHasFood(Ant regularAnt)
        {
            bool hasFood = regularAnt.GetCarryingFood(); //whether the regular ant has food or not

            //if the regular ant has food, remember it's location
            if (hasFood)
            {
                Point regularAntLocation = regularAnt.GetLocation();
                remembersRegularAntCarryingFood = true;
                regularAntCarryingFoodLocation = regularAntLocation;
            }
        }

        /// <summary>
        /// A bandit ant instance gains knowledge of a regular ant carrying food
        /// </summary>
        /// <param name="newRegularAntCarryingFoodLocation">The location of a regular ant object carrying food, passed by a different bandit ant instance</param>
        public void LearnRegularAntCarryingFood(Point newRegularAntCarryingFoodLocation)
        {
            remembersRegularAntCarryingFood = true;
            regularAntCarryingFoodLocation = newRegularAntCarryingFoodLocation;
        }

        /// <summary>
        /// Setter method for a bandit ant instance learning the coordinates of a bandit nest object
        /// </summary>
        /// <param name="location">Coordinates of a bandit nest object</param>
        public void LearnBanditNest(Point newBanditNestLocation)
        {
            remembersBanditNest = true;
            banditNestLocation = newBanditNestLocation;
        }

        /// <summary>
        /// Setter method for a bandit ant instance forgetting last known coordinates of a regular ant carrying food 
        /// </summary>
        public void ForgetRegularAntCarryingFood()
        {
            remembersRegularAntCarryingFood = false;
        }
        /// <summary>
        /// Setter method for a regular ant instance forgetting last known bandit nest object coordinates
        /// </summary>
        public void ForgetBanditNest()
        {
            remembersBanditNest = false;
        }

        /// <summary>
        /// Getter method
        /// </summary>
        /// <returns>Current location of a bandit ant instance</returns>
        public Point GetLocation()
        {
            return location;
        }

        /// <summary>
        /// Getter method
        /// </summary>
        /// <returns>Whether a bandit ant instance knows where a regular ant object carrying food is or not</returns>
        public bool GetRegularAntCarryingFoodKnowledge()
        {
            return remembersRegularAntCarryingFood;
        }

        /// <summary>
        /// Getter Method
        /// </summary>
        /// <returns>Whether a bandit ant instance knows where a bandit nest object is or not</returns>
        public bool GetBanditNestKnowledge()
        {
            return remembersBanditNest;
        }

        /// <summary>
        /// Getter method
        /// </summary>
        /// <returns>A bandit ant instance's last known coordinates of a regular ant object carrying food</returns>
        public Point GetFood()
        {
            return regularAntCarryingFoodLocation;
        }

        /// <summary>
        /// Getter method
        /// </summary>
        /// <returns>A bandit ant instance's last known bandit nest object coordinates</returns>
        public Point GetBanditNest()
        {
            return banditNestLocation;
        }

        /// <summary>
        /// Getter method
        /// </summary>
        /// <returns>Whether a bandit ant instance is carrying STOLEN food or not</returns>
        public bool GetCarryingFood()
        {
            return isCarryingFood;
        }

        /// <summary>
        /// Getter method
        /// </summary>
        /// <returns>A bandit ant instance's current memory counter to determine what knowledge to forget</returns>
        public int GetMemory()
        {
            return antMemory;
        }
    }
}
