﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntProject
{
    /// <summary>
    /// An ant that the AntForm class creates a set amount of instances of on application start;
    /// Each ant instance will move in one of 8 surrounding directions every time step;
    /// The ant instance starts out with no knowledge of the location of any foodpile or nest objects and isn't carrying any food;
    /// The ant instance moves around the world randomly until it comes into detection range of a foodpile or nest object, 
    /// in which case it will remember the location;
    /// When an ant instance is in range of another ant instance, the two instances will share whatever knowledge they have of existing
    /// foodpile or nest object locations;
    /// When an ant instance isn't carrying food, it will search for a foodpile object, or return to one it knows the location of to pick up food;
    /// When an ant instance is carrying food, it will search for a nest object, or return to one it knows the location of to drop off the food;
    /// </summary>
    class Ant
    {

        private Point location; //the x and y position of the ant instance

        //tile size and world size passed from AntForm
        private int tile;
        private int widthBoundary;
        private int heightBoundary;

        //booleans for determining ant instance's current goal (find food or find nest)
        private bool isCarryingFood = false;
        private bool remembersFood = false;
        private bool remembersNest = false;

        //instance's last known coordinates of food and nest objects
        private Point foodLocation;
        private Point nestLocation;


        private int currentDirection;//which of the 8 directions an ant instance is currently moving in

        //the eight directions mapped to numeric values so that they can be incremented and decremented in
        private int north = 1, northeast = 2, east = 3, southeast = 4, south = 5, southwest = 6, west = 7, northwest = 8;


        private int antMemory = 3;//a counter that determines what a randomly picked ant instance will forget based on its value


        private int radius = 5;//the maximum number of tiles two ant instances can have between them and still communicate what they know
        private int detectRadius = 10; //the maximum number of tiles an ant instance can be detected from by a bandit ant

       /* Construct the Ant object:
        * An ant instance is passed the current tileSize and world size of the world it was initialised in
        * as well as randomised x and y coordinates.
        */
        public Ant(int tileSize, int worldWidth, int worldHeight, int xPos, int yPos)
        {
            tile = tileSize;
            widthBoundary = worldWidth;
            heightBoundary = worldHeight;

            location.X = xPos;
            location.Y = yPos;
        }

        /// <summary>
        /// The method called by AntForm to simulate an ant instance's behaviour in one time step;
        /// Cycle's ant instance's memory counter to determine what it forgets if it is randomly picked by AntForm to forget something;
        /// Tells an ant instance that if it is at the known location of a foodpile and hasn't picked anything up, the foodpile is no longer there;
        /// Determines whether an ant instance should face towards a known foodpile, face towards a known nest or search for one of those by facing in random directions,
        /// and determines what direction an ant instance should face towards in order to do so;
        /// Moves the ant in the direction it is facing;
        /// </summary>
        /// <param name="randomVal">
        /// Random value passed from AntForm rather that created in Ant class so that every ant instance has a unique random seed
        /// </param>
        public void Simulate(int randomVal)
        {

            antMemory--;//ant memory decreases

            
            if (antMemory == 0)//if ant memory reaches zero cycle it back around
            {
                antMemory = 3;
            }

            /*if an ant instance is within a tile of the food location they remember, and they haven't picked up food
            then assume there is no food left, forget the location and move on*/
            if (location.X <= foodLocation.X + tile && location.X > foodLocation.X - tile)
            {
                if (location.Y < foodLocation.Y + tile && location.Y > foodLocation.Y - tile)
                {
                    if (isCarryingFood == false)
                    {
                        remembersFood = false;
                    }
                }
            }

            //if an ant instance is not carrying food but remembers where a foodpile object is, move towards the food
            if (isCarryingFood == false && remembersFood == true)
            {
                MoveTowards(foodLocation.X, foodLocation.Y, randomVal);//pass the location of the foodpile and the randomVal object on
            }
            //if an ant instance is carrying food and remembers where a nest object is, move towards the nest
            else if (isCarryingFood == true && remembersNest == true)
            {
                MoveTowards(nestLocation.X, nestLocation.Y, randomVal);//pass the location of the target and randomVal object on
            }
            //if an ant instance doesn't know where to go next, just move in a random direction until it finds something
            else
            {
                RandomMovement(randomVal);
            }

        }

        /// <summary>
        /// Changes an ant instance's current direction so that they are roughly facing their target;
        /// </summary>
        /// <param name="xPos">x coordinate of the target</param>
        /// <param name="yPos">y coordinate of the target</param>
        /// <param name="randomVal">Random modifier to cycle the direction up or down one, or stay the same</param>
        public void MoveTowards(int xPos, int yPos, int randomVal)
        {
            
            /*
             an ant instance's currentDirection is set to one of three directions:
             * exact direction towards target (randomVal == 0)
             * direction towards target but slightly off to the left (randomVal == -1)
             * direction towards target but slightly off to the right (randomVal == 1)
            */
            //if target is southeast
            if (location.X < xPos && location.Y < yPos)
            {
                currentDirection = southeast + randomVal;
            }
            //if target is south 
            else if (location.X == xPos && location.Y < yPos)
            {
                currentDirection = south + randomVal;
            }
            //if target is southwest
            else if (location.X > xPos && location.Y < yPos)
            {
                currentDirection = southwest + randomVal;
            }
            //if target is west
            else if (location.X > xPos && location.Y == yPos)
            {
                currentDirection = west + randomVal;
            }
            //if target is northwest
            else if (location.X > xPos && location.Y > yPos)
            {
                currentDirection = northwest + randomVal;
            }
            //if target is north
            else if (location.X == xPos && location.Y > yPos)
            {
                currentDirection = north + randomVal;
            }
            //if target is northeast
            else if (location.X < xPos && location.Y > yPos)
            {
                currentDirection = northeast + randomVal;
            }
            //if target is east
            else if (location.X < xPos && location.Y == yPos)
            {
                currentDirection = east + randomVal;
            }

            //complete circle of directions
            if (currentDirection < north)
            {
                currentDirection = northwest;
            }
            else if (currentDirection > northwest)
            {
                currentDirection = north;
            }


            Move();//move in direction decided
            
        }

        /// <summary>
        /// Increments or decrements an ant instance's current direction, or keeps it the same to emulate more natural random movement
        /// rather than just having the ant instance move randomly in any of eight directions regardless of where it was last going
        /// </summary>
        /// <param name="randomVal">random modifier determine whether to increment or decrement current direction, or stay the same</param>
        public void RandomMovement(int randomVal)
        {
            //add 1, 0 or -1 to currentDirection to adjust it slightly left, stay on course or adjust slightly right
            currentDirection += randomVal;

            //complete circle of eight directions
            if (currentDirection < north)
            {
                currentDirection = northwest;
            }
            if (currentDirection > northwest)
            {
                currentDirection = north;
            }


            Move();//move in direction decided
        }

        /// <summary>
        /// Move an ant instance forward in whatever of the eight possible directions it is currently facing;
        /// If an ant instance moves off the edge of the screen, wrap it around to the other side.
        /// </summary>
        public void Move()
        {
            //change an ant instance's position coordinates based on the current direction it is facing in
            switch (currentDirection)
            {
                //move north
                case 1:
                    location.Y -= tile;
                    break;
                //move northwest
                case 2:
                    location.Y -= tile;
                    location.X += tile;
                    break;
                //move west
                case 3:
                    location.X += tile;
                    break;
                //move southwest
                case 4:
                    location.X += tile;
                    location.Y += tile;
                    break;
                //move south
                case 5:
                    location.Y += tile;
                    break;
                //move southeast
                case 6:
                    location.Y += tile;
                    location.X -= tile;
                    break;
                //move east
                case 7:
                    location.X -= tile;
                    break;
                //move northeast
                case 8:
                    location.X -= tile;
                    location.Y -= tile;
                    break;
            }

            //if an ant instance moves off the edge of the world, wrap it around to the other side
            if (location.X < 0)
            {
                location.X = widthBoundary - tile;
            }
            if (location.X == widthBoundary)
            {
                location.X = 0;
            }
            if (location.Y < 0)
            {
                location.Y = heightBoundary - tile;
            }
            if (location.Y == heightBoundary)
            {
                location.Y = 0;
            }
        }

        /// <summary>
        /// Compares the distance between two ant instances and returns if they are within radius of each other
        /// </summary>
        /// <param name="otherAnt">The other ant instance</param>
        public bool isInRadius(Ant otherAnt)
        {
            //the distance between this ant instance and the other
            Point otherAntLocation = otherAnt.GetLocation();
            int distX = (location.X - otherAntLocation.X) / tile;
            int distY = (location.Y - otherAntLocation.Y) / tile;

            //if the distance between ant instances is less than the radius
            if (distX <= radius && distX > -radius && distY <= radius && distY > -radius)
            {

                return true;//the ant instances are in radius of each other
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Compares the distance between an ant instance and a food pile object and determines if the ant is in the food pile's radius
        /// </summary>
        public bool isInRadius(FoodPile foodPile)
        {
            
            Point foodLocation = foodPile.GetLocation();//the food pile object's coordinates

            //the distance between this ant instance and the pile
            int distX = (location.X - foodLocation.X) / tile;
            int distY = (location.Y - foodLocation.Y) / tile;

            //if the distance between ant and pile is less than the pile object's radius
            if (distX <= foodPile.GetRadius() && distX > -foodPile.GetRadius() && distY <= foodPile.GetRadius() && distY > -foodPile.GetRadius())
            {

                return true;//the ant is in the pile's radius
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Compares the distance between an ant instance and a foodpile object and determines
        /// whether they are within one tile of each other
        /// </summary>
        public bool isTouching(FoodPile foodPile)
        {

            Point foodLocation = foodPile.GetLocation();//the food pile object's coordinates

            //the distance between this ant and the pile
            int distX = (location.X - foodLocation.X) / tile;
            int distY = (location.Y - foodLocation.Y) / tile;

            //if the distance between ant and pile is less than one tile
            if (distX <= tile && distX > -tile && distY <= tile && distY > -tile)
            {

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Compares the distance between an ant instance and a nest object and determines if the ant is in the nest's radius
        /// </summary>
        public bool isInRadius(Nest nest)
        {
            //determine the distance between this ant and the nest
            Point nestLocation = nest.GetLocation();
            int distX = (location.X - nestLocation.X) / tile;
            int distY = (location.Y - nestLocation.Y) / tile;

            //if the distance between the ant and the nest is less than the radius
            if (distX <= nest.GetRadius() && distX > -nest.GetRadius() && distY <= nest.GetRadius() && distY > -nest.GetRadius())
            {
                return true;//the ant is in the nest's radius
            }
            else
            {
                return false;
            }
            
        }


        /// <summary>
        /// Compares the distance between an ant instance and a nest object and determines
        /// whether they are within one tile of each other
        /// </summary>
        public bool isTouching(Nest nest)
        {
            //the distance between this ant and the nest
            Point nestLocation = nest.GetLocation();
            int distX = (location.X - nestLocation.X) / tile;
            int distY = (location.Y - nestLocation.Y) / tile;

            //if the distance between the ant and the nest is less than a tile
            if (distX <= tile && distX > -tile && distY <= tile && distY > -tile)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// One ant instance tells another the location of a foodpile and/or nest object if one knows the location of one
        /// and the other doesn't.
        /// </summary>
        /// <param name="otherAnt">Another ant instance</param>
        public void Communicate(Ant otherAnt)
        {
            /* if this ant instance knows where food is
             * and the other ant instance doesn't
             */
            if (remembersFood == true)
            {
                if (otherAnt.GetFoodKnowledge() == false)
                {
                    /*
                     * If the food location this ant instance knows isn't the last location the other ant instance remembered, teach it;
                     * (Stops the other ant instance from following this one to a food pile that it just found out no longer exists there).
                     */
                    if (otherAnt.GetFood() != foodLocation)
                    {
                        otherAnt.LearnFood(foodLocation);//access setter method for foodpile object coordinates in the other ant instance
                    }
                }
            }
            //if this ant instance doesn't know where food is but the other ant instance does
            else
            {
                if (otherAnt.GetFoodKnowledge() == true)
                {
                    /*
                     * If the food location the other ant instance knows isn't the last location this ant instance remembered, learn from it;
                     * (Stops this ant instance from following the other one to a food pile that it just found out no longer exists there).
                     */
                    if (otherAnt.GetFood() != foodLocation)
                    {

                        remembersNest = true;
                        
                        foodLocation = otherAnt.GetFood(); //access getter method for foodpile object coordinates from other ant instance
                    }
                }
            }

            //if this ant instance knows the location of a nest object and the other ant instance doesn't, teach it
            if (remembersNest == true)
            {
                if (otherAnt.GetNestKnowledge() == false)
                {
                    otherAnt.LearnNest(nestLocation);//access setter method for nest object coordinates in the other ant instance
                }
            }
            //if this ant doesn't know where the nest is but the other ant instance does, learn from it
            else
            {
                if (otherAnt.GetNestKnowledge() == true)
                {
                    remembersNest = true;

                    nestLocation = otherAnt.GetNest();//access getter method for nest object coordinates in other ant instance
                }
            }
        }

        /// <summary>
        /// Setter method for an ant instance picking up food
        /// </summary>
        public void PickedUpFood()
        {
            isCarryingFood = true;
        }

        /// <summary>
        /// Setter method for an ant instance losing food
        /// </summary>
        public void DropFood()
        {
            isCarryingFood = false;
        }

        /// <summary>
        /// Setter method for an ant instance learning the coordinates of a food pile object
        /// </summary>
        /// <param name="location">Coordinates of a food pile object</param>
        public void LearnFood(Point newFoodLocation)
        {
            remembersFood = true;
            foodLocation = newFoodLocation;
        }

        /// <summary>
        /// Setter method for an ant instance learning the coordinates of a nest object
        /// </summary>
        /// <param name="location">Coordinates of a nest object</param>
        public void LearnNest(Point newNestLocation)
        {
            remembersNest = true;
            nestLocation = newNestLocation;
        }

        /// <summary>
        /// Setter method for an ant instance forgetting last known foodpile object coordinates
        /// </summary>
        public void ForgetFood()
        {
            remembersFood = false;
        }
        /// <summary>
        /// Setter method for an ant instance forgetting last know nest object coordinates
        /// </summary>
        public void ForgetNest()
        {
            remembersNest = false;
        }

        /// <summary>
        /// Getter method
        /// </summary>
        /// <returns>Current location of an ant instance</returns>
        public Point GetLocation()
        {
            return location;
        }

        /// <summary>
        /// Getter method
        /// </summary>
        /// <returns>Radius of the ant can be detected by bandit ant from</returns>
        public int GetDetectRadius()
        {
            return detectRadius;
        }

        /// <summary>
        /// Getter method
        /// </summary>
        /// <returns>Whether an ant instance knows where a foodpile object is or not</returns>
        public bool GetFoodKnowledge()
        {
            return remembersFood;
        }

        /// <summary>
        /// Getter Method
        /// </summary>
        /// <returns>Whether an ant instance knows where a nest object is or not</returns>
        public bool GetNestKnowledge()
        {
            return remembersNest;
        }

        /// <summary>
        /// Getter method
        /// </summary>
        /// <returns>An ant instance's last known foodpile object coordinates</returns>
        public Point GetFood()
        {
            return foodLocation;
        }

        /// <summary>
        /// Getter method
        /// </summary>
        /// <returns>An ant instance's last known nest object coordinates</returns>
        public Point GetNest()
        {
            return nestLocation;
        }

        /// <summary>
        /// Getter method
        /// </summary>
        /// <returns>Whether an ant instance is carrying food or not</returns>
        public bool GetCarryingFood()
        {
            return isCarryingFood;
        }

        /// <summary>
        /// Getter method
        /// </summary>
        /// <returns>An ant instance's current memory counter to determine what knowledge to forget</returns>
        public int GetMemory()
        {
            return antMemory;
        }
    }
}
