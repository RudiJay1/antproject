﻿namespace AntProject
{
    partial class AntForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.worldPanel = new System.Windows.Forms.Panel();
            this.toolPanel = new System.Windows.Forms.Panel();
            this.toggleGrid = new System.Windows.Forms.Button();
            this.helpButton = new System.Windows.Forms.Button();
            this.nestButton = new System.Windows.Forms.Button();
            this.foodButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.startButton = new System.Windows.Forms.Button();
            this.banditNestButton = new System.Windows.Forms.Button();
            this.toolPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // worldPanel
            // 
            this.worldPanel.BackColor = System.Drawing.Color.Cornsilk;
            this.worldPanel.Location = new System.Drawing.Point(0, 0);
            this.worldPanel.Margin = new System.Windows.Forms.Padding(4);
            this.worldPanel.MaximumSize = new System.Drawing.Size(1067, 738);
            this.worldPanel.MinimumSize = new System.Drawing.Size(1067, 738);
            this.worldPanel.Name = "worldPanel";
            this.worldPanel.Size = new System.Drawing.Size(1067, 738);
            this.worldPanel.TabIndex = 0;
            // 
            // toolPanel
            // 
            this.toolPanel.BackColor = System.Drawing.Color.Silver;
            this.toolPanel.Controls.Add(this.banditNestButton);
            this.toolPanel.Controls.Add(this.toggleGrid);
            this.toolPanel.Controls.Add(this.helpButton);
            this.toolPanel.Controls.Add(this.nestButton);
            this.toolPanel.Controls.Add(this.foodButton);
            this.toolPanel.Controls.Add(this.stopButton);
            this.toolPanel.Controls.Add(this.startButton);
            this.toolPanel.Location = new System.Drawing.Point(1065, 0);
            this.toolPanel.Margin = new System.Windows.Forms.Padding(4);
            this.toolPanel.MinimumSize = new System.Drawing.Size(53, 738);
            this.toolPanel.Name = "toolPanel";
            this.toolPanel.Size = new System.Drawing.Size(101, 738);
            this.toolPanel.TabIndex = 0;
            // 
            // toggleGrid
            // 
            this.toggleGrid.Location = new System.Drawing.Point(18, 633);
            this.toggleGrid.Name = "toggleGrid";
            this.toggleGrid.Size = new System.Drawing.Size(75, 44);
            this.toggleGrid.TabIndex = 5;
            this.toggleGrid.Text = "Toggle Grid";
            this.toggleGrid.UseVisualStyleBackColor = true;
            this.toggleGrid.Click += new System.EventHandler(this.toggleGrid_Click);
            // 
            // helpButton
            // 
            this.helpButton.Location = new System.Drawing.Point(18, 683);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(75, 35);
            this.helpButton.TabIndex = 4;
            this.helpButton.Text = "Help?";
            this.helpButton.UseVisualStyleBackColor = true;
            this.helpButton.Click += new System.EventHandler(this.helpButton_Click);
            // 
            // nestButton
            // 
            this.nestButton.Location = new System.Drawing.Point(6, 230);
            this.nestButton.Margin = new System.Windows.Forms.Padding(4);
            this.nestButton.Name = "nestButton";
            this.nestButton.Size = new System.Drawing.Size(90, 44);
            this.nestButton.TabIndex = 3;
            this.nestButton.Text = "Ant Nest";
            this.nestButton.UseVisualStyleBackColor = true;
            this.nestButton.Click += new System.EventHandler(this.nestButton_Click);
            // 
            // foodButton
            // 
            this.foodButton.Enabled = false;
            this.foodButton.Location = new System.Drawing.Point(6, 181);
            this.foodButton.Margin = new System.Windows.Forms.Padding(4);
            this.foodButton.Name = "foodButton";
            this.foodButton.Size = new System.Drawing.Size(90, 41);
            this.foodButton.TabIndex = 2;
            this.foodButton.Text = "Food";
            this.foodButton.UseVisualStyleBackColor = true;
            this.foodButton.Click += new System.EventHandler(this.foodButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.Enabled = false;
            this.stopButton.Location = new System.Drawing.Point(7, 57);
            this.stopButton.Margin = new System.Windows.Forms.Padding(4);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(89, 37);
            this.stopButton.TabIndex = 1;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(7, 13);
            this.startButton.Margin = new System.Windows.Forms.Padding(4);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(89, 37);
            this.startButton.TabIndex = 0;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // banditNestButton
            // 
            this.banditNestButton.Location = new System.Drawing.Point(7, 282);
            this.banditNestButton.Margin = new System.Windows.Forms.Padding(4);
            this.banditNestButton.Name = "banditNestButton";
            this.banditNestButton.Size = new System.Drawing.Size(89, 47);
            this.banditNestButton.TabIndex = 6;
            this.banditNestButton.Text = "Bandit Ant Nest";
            this.banditNestButton.UseVisualStyleBackColor = true;
            this.banditNestButton.Click += new System.EventHandler(this.banditNestButton_Click);
            // 
            // AntForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1166, 730);
            this.Controls.Add(this.toolPanel);
            this.Controls.Add(this.worldPanel);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1134, 775);
            this.Name = "AntForm";
            this.Text = "SimAnt 2016";
            this.toolPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel worldPanel;
        private System.Windows.Forms.Panel toolPanel;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Button nestButton;
        private System.Windows.Forms.Button foodButton;
        private System.Windows.Forms.Button helpButton;
        private System.Windows.Forms.Button toggleGrid;
        private System.Windows.Forms.Button banditNestButton;
    }
}