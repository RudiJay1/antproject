# README #

Thank you for looking at my repository for my ant simulator project.

### What is this project? ###

C# project using Visual Studio 2013 to simulate a colony of ants. 

Ants will wander until they find food and a nest, at which point they will go back and forth taking food to the nest until it runs out. They will communicate the locations of the nests and food they know to ants they come across. Sometimes they will forget a location.

Red bandit ants are a separate breed with their own nests. They steal food from regular ants and remember their location in the hopes another ant will be there. They similarly communicate this location to fellow ants.

### How do I get set up? ###

1. Clone the repo.
2. In the repo folder, open AntProject.sln in Visual Studio.
3. Run the project.

### Where can I contact the author? ###

http://www.rudiprentice.com/contact.html